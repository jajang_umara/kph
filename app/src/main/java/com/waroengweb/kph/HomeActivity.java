package com.waroengweb.kph;

import android.Manifest;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.waroengweb.kph.app.AppController;
import com.waroengweb.kph.app.NetworkHelper;
import com.waroengweb.kph.app.VolleyMultipartRequest;
import com.waroengweb.kph.config.Config;
import com.waroengweb.kph.config.Session;
import com.waroengweb.kph.database.AppDatabase;
import com.waroengweb.kph.database.entity.Kph;
import com.waroengweb.kph.fragments.DataFragment;
import com.waroengweb.kph.fragments.HomeFragment;
import com.waroengweb.kph.fragments.InputFragment;
import com.waroengweb.kph.fragments.OnlineFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private ProgressDialog pd;
    private AppDatabase db;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    loadFragment(new InputFragment());
                    return true;
                case R.id.navigation_offline:
                    loadFragment(new DataFragment());
                    return true;
                case R.id.navigation_online:
                    loadFragment(new OnlineFragment());
                    return true;
                case R.id.navigation_upload:
                    uploadData();
                    return true;
                case R.id.navigation_exit:
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            HomeActivity.this);

                    // set title
                    alertDialogBuilder.setTitle("Pertanyaan");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Klik Ya Untuk keluar")
                            .setCancelable(false)
                            .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().commit();
                                    android.os.Process.killProcess(android.os.Process.myPid());
                                    System.exit(1);
                                }
                            })
                            .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        pd = new ProgressDialog(this);
        pd.setMessage("PROSES UPLOADING DATA...");

        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "MyDB").allowMainThreadQueries().fallbackToDestructiveMigration().build();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.ACCESS_WIFI_STATE },
                    105);
        }

        loadFragment(new HomeFragment());
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void uploadData() {

        if(!NetworkHelper.isNetworkAvailable(this)){
            Toast.makeText(this,"Tidak Ada jaringan",Toast.LENGTH_SHORT).show();
        } else {
            if (!NetworkHelper.isInternetAvailable()){
                Toast.makeText(this,"Koneksi Internet bermasalah",Toast.LENGTH_SHORT).show();
            } else {
                pd.show();
                final List<Kph> listKph;
                listKph = db.KphDao().getAllKphByUploaded(0, Session.getUserId(getApplicationContext()));

                VolleyMultipartRequest mReq = new VolleyMultipartRequest(Request.Method.POST, Config.BASE_URL + "api/save_kph",
                        new Response.Listener<NetworkResponse>() {
                            @Override
                            public void onResponse(NetworkResponse response) {
                                pd.dismiss();
                                AppController.getInstance().getRequestQueue().getCache().clear();

                                if (response.statusCode == 200){
                                    String result = new String(response.data);
                                    //Toast.makeText(getApplicationContext(),result,Toast.LENGTH_SHORT).show();

                                    try {
                                        JSONObject json = new JSONObject(result);
                                        if (json.getString("result").equals("success")){
                                            Toast.makeText(getApplicationContext(),"Data Berhasil Diupload",Toast.LENGTH_SHORT).show();
                                            //db.WargaDao().deleteById(warga.getId());
                                            db.KphDao().updateKph();




                                        } else {
                                            Toast.makeText(getApplicationContext(),result,Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e){
                                        e.printStackTrace();
                                    }


                                    Log.d("response :",new String(response.data));
                                } else {
                                    Toast.makeText(getApplicationContext(),"ERROR",Toast.LENGTH_SHORT).show();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();

                        if(error.networkResponse.data!=null) {
                            String body;
                            try {
                                body = new String(error.networkResponse.data,"UTF-8");
                                Toast.makeText(getApplicationContext(),body,Toast.LENGTH_SHORT).show();
                                Log.d("Error :",body);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }){
                    @Override
                    protected Map<String,String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        Gson gson = new Gson();
                        params.put("data",gson.toJson(listKph));
                        params.put("user_id",String.valueOf(Session.getUserId(getApplicationContext())));
                        return params;
                    }

                    @Override
                    protected Map<String, DataPart> getByteData() throws AuthFailureError {
                        Map<String, DataPart> params = new HashMap<>();

                        for (Kph kph : listKph){
                            long imagename = System.currentTimeMillis();


                            if (kph.getFoto1() != null){
                                params.put("photo1["+kph.getId()+"]", new DataPart("PIC_1_"+String.valueOf(imagename)+".jpg",convertImageToBytes(kph.getFoto1())));
                            }

                            if (kph.getFoto2() != null){
                                params.put("photo2["+kph.getId()+"]", new DataPart("PIC_2_"+String.valueOf(imagename)+".jpg",convertImageToBytes(kph.getFoto2())));
                            }

                            if (kph.getFoto3() != null){
                                params.put("photo3["+kph.getId()+"]", new DataPart("PIC_3_"+String.valueOf(imagename)+".jpg",convertImageToBytes(kph.getFoto3())));
                            }

                        }

                        return params;
                    }
                };

                mReq.setRetryPolicy(new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


                AppController.getInstance().addToRequestQueue(mReq);
            }
        }


    }

    public  byte[] convertImageToBytes(String path) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

}
