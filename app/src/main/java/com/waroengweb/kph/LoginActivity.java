package com.waroengweb.kph;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.waroengweb.kph.app.AppController;
import com.waroengweb.kph.app.NetworkHelper;
import com.waroengweb.kph.config.Config;
import com.waroengweb.kph.config.Session;
import com.waroengweb.kph.model.LoginResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private AwesomeValidation awesomeValidation;
    @BindView(R.id.username) EditText username;
    @BindView(R.id.password) EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        Intent i;
        /*Session.setLoginStatus(LoginActivity.this, true);
        Session.setUserId(LoginActivity.this, 1);
        Session.setRole(LoginActivity.this, "admin");
        Session.setFullname(LoginActivity.this, "jajang"); */
        if (Session.getLoginStatus(LoginActivity.this)){
            i = new Intent(LoginActivity.this,HomeActivity.class);
            startActivity(i);
            finish();
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.ACCESS_WIFI_STATE },
                    105);
        }

        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(LoginActivity.this,R.id.username, RegexTemplate.NOT_EMPTY,R.string.nameerror);
        awesomeValidation.addValidation(LoginActivity.this,R.id.password, RegexTemplate.NOT_EMPTY,R.string.nameerror);
    }

    @OnClick(R.id.btn_login)
    public void login()
    {
        if(!NetworkHelper.isNetworkAvailable(this)){
            Toast.makeText(this,"Tidak Ada jaringan",Toast.LENGTH_SHORT).show();
        } else {
            if (!NetworkHelper.isInternetAvailable()) {
                Toast.makeText(this, "Koneksi Internet bermasalah", Toast.LENGTH_SHORT).show();
            } else {
                if (awesomeValidation.validate()) {

                    progressDialog.show();
                    StringRequest strReq = new StringRequest(Request.Method.POST, Config.BASE_URL + "api/login", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            JSONObject jsonObject = null;
                            //Toast.makeText(getApplicationContext(),response,Toast.LENGTH_SHORT).show();

                            try {
                                jsonObject = new JSONObject(response);

                                if (jsonObject.getString("result").equals("success")) {

                                    if (jsonObject.getString("role").equals("petugas")) {
                                        Session.setLoginStatus(LoginActivity.this, true);
                                        Session.setUserId(LoginActivity.this, jsonObject.getInt("id"));
                                        Session.setRole(LoginActivity.this, jsonObject.getString("role"));
                                        Session.setFullname(LoginActivity.this, jsonObject.getString("fullname"));

                                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Untuk Pengguna selain petugas silakan kunjungi kphpkerinci.id", Toast.LENGTH_SHORT).show();
                                    }


                                } else {
                                    Toast.makeText(getApplicationContext(), "Nama User atau Password Salah", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            progressDialog.hide();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            progressDialog.hide();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();

                            params.put("username", username.getText().toString());
                            params.put("password", password.getText().toString());

                            return params;
                        }

                    };

                    AppController.getInstance().addToRequestQueue(strReq, "Str Req");


                }
            }
        }
    }
}
