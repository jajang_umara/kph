package com.waroengweb.kph;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.waroengweb.kph.app.AppController;
import com.waroengweb.kph.app.VolleyMultipartRequest;
import com.waroengweb.kph.config.Config;
import com.waroengweb.kph.config.Session;
import com.waroengweb.kph.database.AppDatabase;
import com.waroengweb.kph.database.entity.Kph;
import com.waroengweb.kph.fragments.DataFragment;
import com.waroengweb.kph.fragments.DataOnlineFragment;
import com.waroengweb.kph.fragments.HomeFragment;
import com.waroengweb.kph.fragments.InputFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private AppDatabase db;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = (TextView) headerView.findViewById(R.id.textView);
        navUsername.setText("Selamat Datang : "+ Session.getFullname(MainActivity.this));

        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "MyDB").allowMainThreadQueries().fallbackToDestructiveMigration().build();

        pd = new ProgressDialog(this);
        pd.setMessage("PROSES UPLOADING DATA...");

        if (!Session.getRole(MainActivity.this).equals("petugas")){
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_input).setVisible(false);
            nav_Menu.findItem(R.id.nav_data).setVisible(false);
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame,new HomeFragment());
        ft.addToBackStack(null);
        ft.commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
           uploadData();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;

        if (id == R.id.nav_input) {
            fragment = new InputFragment();
        } else if (id == R.id.nav_data) {
            fragment = new DataFragment();
        } else if (id == R.id.nav_data_online) {
            fragment = new DataOnlineFragment();
        }  else if (id == R.id.nav_exit) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    MainActivity.this);

            // set title
            alertDialogBuilder.setTitle("Pertanyaan");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Klik Ya Untuk keluar")
                    .setCancelable(false)
                    .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().commit();
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                        }
                    })
                    .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }

        if (fragment != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame,fragment);
            ft.addToBackStack(null);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void uploadData() {
        pd.show();
        final List<Kph> listKph;
        listKph = db.KphDao().getAllKphByUploaded(0,Session.getUserId(getApplicationContext()));

        VolleyMultipartRequest mReq = new VolleyMultipartRequest(Request.Method.POST, Config.BASE_URL + "api/save_kph",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        pd.dismiss();
                        AppController.getInstance().getRequestQueue().getCache().clear();

                        if (response.statusCode == 200){
                            String result = new String(response.data);
                            //Toast.makeText(getApplicationContext(),result,Toast.LENGTH_SHORT).show();

                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString("result").equals("success")){
                                    Toast.makeText(getApplicationContext(),"Data Berhasil Diupload",Toast.LENGTH_SHORT).show();
                                    //db.WargaDao().deleteById(warga.getId());
                                    db.KphDao().updateKph();




                                } else {
                                    Toast.makeText(getApplicationContext(),result,Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e){
                                e.printStackTrace();
                            }


                            Log.d("response :",new String(response.data));
                        } else {
                            Toast.makeText(getApplicationContext(),"ERROR",Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();

                if(error.networkResponse.data!=null) {
                    String body;
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");
                        Toast.makeText(getApplicationContext(),body,Toast.LENGTH_SHORT).show();
                        Log.d("Error :",body);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

            }
        }){
            @Override
            protected Map<String,String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                Gson gson = new Gson();
                params.put("data",gson.toJson(listKph));
                params.put("user_id",String.valueOf(Session.getUserId(getApplicationContext())));
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> params = new HashMap<>();

                for (Kph kph : listKph){
                    long imagename = System.currentTimeMillis();


                    if (kph.getFoto1() != null){
                        params.put("photo1["+kph.getId()+"]", new DataPart("PIC_1_"+String.valueOf(imagename)+".jpg",convertImageToBytes(kph.getFoto1())));
                    }

                    if (kph.getFoto2() != null){
                        params.put("photo2["+kph.getId()+"]", new DataPart("PIC_2_"+String.valueOf(imagename)+".jpg",convertImageToBytes(kph.getFoto2())));
                    }

                    if (kph.getFoto3() != null){
                        params.put("photo3["+kph.getId()+"]", new DataPart("PIC_3_"+String.valueOf(imagename)+".jpg",convertImageToBytes(kph.getFoto3())));
                    }

                }

                return params;
            }
        };

        mReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        AppController.getInstance().addToRequestQueue(mReq);
    }

    public  byte[] convertImageToBytes(String path) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }
}
