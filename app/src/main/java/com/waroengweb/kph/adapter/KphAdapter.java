package com.waroengweb.kph.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.waroengweb.kph.R;
import com.waroengweb.kph.database.entity.Kph;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KphAdapter extends RecyclerView.Adapter<KphAdapter.MyViewHolder> {

    public static List<Kph> kphList;
    private Context context;

    public KphAdapter(Context context,List<Kph> kphList){
        this.context = context;
        this.kphList = kphList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card_view) CardView cv;
        @BindView(R.id.id) TextView id;
        @BindView(R.id.judul) TextView judul;
        @BindView(R.id.harga) TextView harga;

        @BindView(R.id.thumbnail) ImageView thumbnail;

        public MyViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //holder.mImage.setImageResource(listdata.get(position).getThubnail());
        holder.id.setText(String.valueOf(kphList.get(position).getId()));
        holder.judul.setText(kphList.get(position).getNamaPotensi());
        holder.harga.setText("Qty :"+String.valueOf(kphList.get(position).getQuantity()+" ("+kphList.get(position).getSatuan()+")"));

        final RecyclerView.ViewHolder x=holder;
        Glide.with(context)
                .load(kphList.get(position).getFoto1())
                .into(holder.thumbnail);
        holder.id.setVisibility(View.GONE);
    }



    @Override
    public int getItemCount() {
        return kphList.size();
    }
}
