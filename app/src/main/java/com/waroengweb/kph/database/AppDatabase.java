package com.waroengweb.kph.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.waroengweb.kph.database.dao.KphDao;
import com.waroengweb.kph.database.entity.Kph;

@Database(entities = {Kph.class},version = 2,exportSchema = false)
@TypeConverters({DateTypeConverter.class})

public abstract class AppDatabase extends RoomDatabase {
    public abstract KphDao KphDao();
}
