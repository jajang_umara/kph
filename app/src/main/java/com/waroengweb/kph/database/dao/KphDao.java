package com.waroengweb.kph.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.waroengweb.kph.database.entity.Kph;

import java.util.List;

@Dao
public interface KphDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertKph(Kph kph);

    @Query("SELECT * FROM kph WHERE user_id = :userId ORDER BY tanggal DESC")
    List<Kph> getAllKph(int userId);

    @Query("SELECT * FROM kph WHERE uploaded = :uploaded AND user_id = :userId ORDER BY tanggal DESC")
    List<Kph> getAllKphByUploaded(int uploaded,int userId);

    @Query("SELECT * FROM kph WHERE id IN(:ids)")
    List<Kph> getAllKphByIds(List<Integer> ids);

    @Query("SELECT * FROM kph WHERE id = :id")
    Kph getKphById(int id);

    @Query("DELETE FROM kph WHERE id IN(:ids)")
    void deleteByIds(List<Integer> ids);

    @Query("UPDATE kph SET uploaded=1 WHERE uploaded=0")
    void updateKph();

    @Delete
    void deleteData(Kph kph);
}
