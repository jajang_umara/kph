package com.waroengweb.kph.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName="kph")
public class Kph implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull private int id;
    @ColumnInfo(name="user_id") private int userId;

    @ColumnInfo(name="jenis_potensi") private String jenisPotensi;
    @ColumnInfo(name="nama_potensi") private String namaPotensi;
    @ColumnInfo(name="jenis_latin") private String namaLatin;
    @ColumnInfo(name="tata_blok") private String tataBlok;
    @ColumnInfo(name="kode_blok") private String kodeBlok;

    private Date tanggal;
    private Float quantity;
    private String satuan;
    private Double longitude;
    private Double latitude;
    private Float ketinggian;
    private String desa;
    private String kecamatan;
    private String kabupaten;
    private String foto1,foto2,foto3;
    private int uploaded;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setJenisPotensi(String jenisPotensi) {
        this.jenisPotensi = jenisPotensi;
    }

    public String getJenisPotensi() {
        return jenisPotensi;
    }

    public String getNamaPotensi() {
        return namaPotensi;
    }

    public void setNamaPotensi(String namaPotensi) {
        this.namaPotensi = namaPotensi;
    }

    public String getNamaLatin() {
        return namaLatin;
    }

    public void setNamaLatin(String namaLatin) {
        this.namaLatin = namaLatin;
    }

    public String getKodeBlok() {
        return kodeBlok;
    }

    public void setKodeBlok(String kodeBlok) {
        this.kodeBlok = kodeBlok;
    }

    public String getTataBlok() {
        return tataBlok;
    }

    public void setTataBlok(String tataBlok) {
        this.tataBlok = tataBlok;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setKetinggian(Float ketinggian) {
        this.ketinggian = ketinggian;
    }

    public Float getKetinggian() {
        return ketinggian;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Float getQuantity() {
        return quantity;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public void setDesa(String desa) {
        this.desa = desa;
    }

    public String getDesa() {
        return desa;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getFoto1() {
        return foto1;
    }

    public void setFoto1(String foto1) {
        this.foto1 = foto1;
    }

    public String getFoto2() {
        return foto2;
    }

    public void setFoto2(String foto2) {
        this.foto2 = foto2;
    }

    public void setFoto3(String foto3) {
        this.foto3 = foto3;
    }

    public String getFoto3() {
        return foto3;
    }

    public void setUploaded(int uploaded) {
        this.uploaded = uploaded;
    }

    public int getUploaded() {
        return uploaded;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
