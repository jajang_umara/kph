package com.waroengweb.kph.fragments;


import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waroengweb.kph.R;
import com.waroengweb.kph.adapter.KphAdapter;
import com.waroengweb.kph.config.Session;
import com.waroengweb.kph.database.AppDatabase;
import com.waroengweb.kph.database.entity.Kph;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataFragment extends Fragment {

    static RecyclerView recyclerView;
    public static KphAdapter wAdapter;
    public static List<Kph> kphList = new ArrayList<>();
    public static AppDatabase db;
    private Gson gson;
    private GridLayoutManager layoutmanager;




    public DataFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("LIST DATA KPH");
        View view = inflater.inflate(R.layout.fragment_data, container, false);

        ButterKnife.bind(this,view);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutmanager=new GridLayoutManager(getActivity(),2);
        layoutmanager.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutmanager);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Kph kph = kphList.get(position);
                //Gson gson = new Gson();

                //Toast.makeText(getActivity(),gson.toJson(kph),Toast.LENGTH_SHORT).show();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                DialogFragment dialogFragment = new DetailKphFragments(kph);
                dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                dialogFragment.show(ft, "dialog");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        db = Room.databaseBuilder(getActivity(),
                AppDatabase.class, "MyDB").allowMainThreadQueries().fallbackToDestructiveMigration().build();




        prepareKphData(0);

        return view;
    }



   public void prepareKphData(int uploaded) {
        int userId = Session.getUserId(getActivity());
        if (uploaded == 2){
            kphList = db.KphDao().getAllKph(userId);
        } else {
            kphList = db.KphDao().getAllKphByUploaded(uploaded,userId);
        }

        wAdapter = new KphAdapter(getActivity(),kphList);
        recyclerView.setAdapter(wAdapter);
        wAdapter.notifyDataSetChanged();
    }



}
