package com.waroengweb.kph.fragments;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.waroengweb.kph.MainActivity;
import com.waroengweb.kph.R;
import com.waroengweb.kph.database.AppDatabase;
import com.waroengweb.kph.database.entity.Kph;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailKphFragments extends DialogFragment {

    private Kph kph;
    public static AppDatabase db;
    TextView jenisPotensi,namaPotensi,namaLatin;
    @BindView(R.id.quantity) TextView quantity;
    @BindView(R.id.ketinggian) TextView ketinggian;
    @BindView(R.id.desa) TextView desa;
    @BindView(R.id.kecamatan) TextView kecamatan;
    @BindView(R.id.kabupaten) TextView kabupaten;
    @BindView(R.id.tata_blok) TextView tataBlok;
    @BindView(R.id.kode_blok) TextView kodeBlok;
    @BindView(R.id.hapus_button) Button hapusButton;


    public DetailKphFragments(Kph kph){
        this.kph = kph;
    }

    public DetailKphFragments(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("KPH Detail");
        View v = inflater.inflate(R.layout.fragment_detail_kph, container, false);

        ButterKnife.bind(this,v);

        jenisPotensi = (TextView)v.findViewById(R.id.jenis_potensi);
        namaPotensi = (TextView)v.findViewById(R.id.nama_potensi);
        namaLatin = (TextView)v.findViewById(R.id.nama_latin);

        jenisPotensi.setText(kph.getJenisPotensi());
        namaPotensi.setText(kph.getNamaPotensi());
        namaLatin.setText(kph.getNamaLatin());
        quantity.setText(String.valueOf(kph.getQuantity())+" ("+kph.getSatuan()+")");
        ketinggian.setText(String.valueOf(kph.getKetinggian()+" MDPL"));
        desa.setText(kph.getDesa());
        kecamatan.setText(kph.getKecamatan());
        kabupaten.setText(kph.getKabupaten());
        tataBlok.setText(kph.getTataBlok());
        kodeBlok.setText(kph.getKodeBlok());


        if (kph.getUploaded() == 1){
            hapusButton.setVisibility(View.GONE);
        } else {
            hapusButton.setVisibility(View.VISIBLE);
        }

        db = Room.databaseBuilder(getActivity(),
                AppDatabase.class, "MyDB").allowMainThreadQueries().fallbackToDestructiveMigration().build();

        return v;
    }

    @OnClick(R.id.hapus_button)
    void HapusData(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());

        // set title
        alertDialogBuilder.setTitle("Pertanyaan");

        // set dialog message
        alertDialogBuilder
                .setMessage("Apakah Anda Yakin Hapus Data ini?")
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                       db.KphDao().deleteData(kph);
                        new DataFragment().prepareKphData(2);
                        getDialog().dismiss();
                    }
                })
                .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


}
