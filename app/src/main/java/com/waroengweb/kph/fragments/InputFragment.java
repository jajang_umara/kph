package com.waroengweb.kph.fragments;


import android.Manifest;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.waroengweb.kph.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.iceteck.silicompressorr.SiliCompressor;
import com.waroengweb.kph.config.MyLocation;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;
import static com.basgeekball.awesomevalidation.ValidationStyle.TEXT_INPUT_LAYOUT;

import com.waroengweb.kph.R;

import  com.waroengweb.kph.config.MyLocation;
import com.waroengweb.kph.config.Session;
import com.waroengweb.kph.database.AppDatabase;
import com.waroengweb.kph.database.entity.Kph;

/**
 * A simple {@link Fragment} subclass.
 */
public class InputFragment extends Fragment {

    @BindView(R.id.jenis_potensi) AppCompatSpinner jenisPotensi;
	@BindView(R.id.nama_potensi) EditText namaPotensi;
	@BindView(R.id.nama_latin) EditText namaLatin;
    @BindView(R.id.quantity) EditText quantity;
    @BindView(R.id.satuan) EditText satuan;
    @BindView(R.id.ketinggian) EditText ketinggian;
    @BindView(R.id.desa) EditText desa;
    @BindView(R.id.kecamatan) EditText kecamatan;
    @BindView(R.id.kabupaten) EditText kabupaten;
    @BindView(R.id.tata_blok) EditText tataBlok;
    @BindView(R.id.kode_blok) EditText kodeBlok;
    @BindView(R.id.preview_1) ImageView image1;
    @BindView(R.id.preview_2) ImageView image2;
    @BindView(R.id.preview_3) ImageView image3;
    @BindView(R.id.take_picture_1) Button btn1;
    @BindView(R.id.take_picture_2) Button btn2;
    @BindView(R.id.take_picture_3) Button btn3;


    private Uri file1,file2,file3;
    private String fileString1,fileString2,fileString3;
    private AwesomeValidation awesomeValidation;
    private Double longitude,latitude;
    MyLocation myLocation = new MyLocation();
    private AppDatabase db;

    public InputFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("INPUT DATA KPH");
        View view = inflater.inflate(R.layout.fragment_input, container, false);



        ButterKnife.bind(this,view);
        db = Room.databaseBuilder(getActivity(),
                AppDatabase.class, "MyDB").allowMainThreadQueries().fallbackToDestructiveMigration().build();


        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            latitude = getArguments().getDouble("lat");
            longitude = getArguments().getDouble("lng");
        }


    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        awesomeValidation = new AwesomeValidation(TEXT_INPUT_LAYOUT);
        AwesomeValidation.disableAutoFocusOnFirstFailure();
        awesomeValidation.addValidation(getActivity(),R.id.nama_potensi_lbl, RegexTemplate.NOT_EMPTY,R.string.nameerror);
        awesomeValidation.addValidation(getActivity(),R.id.nama_latin_lbl, RegexTemplate.NOT_EMPTY,R.string.nameerror);
    }

    public MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {

        @Override
        public void gotLocation(Location location) {
            // TODO Auto-generated method stub
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            Toast.makeText(getActivity(),"Titik Gps sudah Ditemukan",Toast.LENGTH_SHORT).show();



        }
    };
	
	
	 public String compressImage(Uri fileData){
        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getActivity().getPackageName() + "/media/image");
        if (f.mkdirs() || f.isDirectory()){
            return SiliCompressor.with(getActivity()).compress(fileData.getPath(),f,true);

        }
        return null;
    }

    @OnClick(R.id.take_picture_1)
    public void takePicture1(){
        takePicture(1);
    }

    @OnClick(R.id.take_picture_2)
    public void takePicture2(){
        takePicture(2);
    }

    @OnClick(R.id.take_picture_3)
    public void takePicture3(){
        takePicture(3);
    }

    @OnClick(R.id.btn_simpan_offline)
    public void saveOffline(){
        saveData("offline");
    }

    public void saveData(String mode) {
        if(awesomeValidation.validate()){
            if (longitude == null || latitude == null){
                Toast.makeText(getActivity(),"LOKASI GPS MASIH KOSONG",Toast.LENGTH_SHORT).show();
            } else {
                Kph kph = new Kph();

                kph.setJenisPotensi(jenisPotensi.getSelectedItem().toString());
                kph.setNamaPotensi(namaPotensi.getText().toString());
                kph.setNamaLatin(namaLatin.getText().toString());
                kph.setQuantity(Float.valueOf(quantity.getText().toString()));
                kph.setSatuan(satuan.getText().toString());
                kph.setKetinggian(Float.valueOf(ketinggian.getText().toString()));
                kph.setDesa(desa.getText().toString());
                kph.setKecamatan(kecamatan.getText().toString());
                kph.setKabupaten(kabupaten.getText().toString());
                kph.setTataBlok(tataBlok.getText().toString());
                kph.setKodeBlok(kodeBlok.getText().toString());
                kph.setUploaded(0);
                kph.setFoto1(fileString1);
                kph.setFoto2(fileString2);
                kph.setFoto3(fileString3);
                kph.setLatitude(latitude);
                kph.setLongitude(longitude);
                kph.setUserId(Session.getUserId(getActivity()));

                DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                Date tanggalNew;
                String tanggal = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault()).format(new Date());
                try {
                    tanggalNew = formatter.parse(tanggal);
                    kph.setTanggal(tanggalNew);
                } catch (ParseException pe){
                    pe.printStackTrace();
                }


                if (mode == "offline"){
                    db.KphDao().insertKph(kph);
                    Toast.makeText(getActivity(),"DATA BERHASIL DISIMPAN",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(),"DATA GAGAL DISIMPAN",Toast.LENGTH_SHORT).show();
                }

                latitude = null;
                longitude = null;

                clearForm();


            }
        }
    }

    private void clearForm()
    {
        namaPotensi.setText("");
        namaLatin.setText("");
        quantity.setText("0");
        satuan.setText("");
        ketinggian.setText("0");
        desa.setText("");
        kecamatan.setText("");
        kabupaten.setText("");
        tataBlok.setText("");
        kodeBlok.setText("");

        image1.setImageResource(android.R.color.transparent);
        image2.setImageResource(android.R.color.transparent);
        image3.setImageResource(android.R.color.transparent);

        btn1.setText("Ambil Photo");
        btn2.setText("Ambil Photo");
        btn3.setText("Ambil Photo");
    }

    public void takePicture(int recCode)
    {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent i;
                   i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            if (recCode == 1){
                file1 = Uri.fromFile(getOutputMediaFile(1));
                i.putExtra(MediaStore.EXTRA_OUTPUT, file1);
            } else if (recCode == 2){
                file2 = Uri.fromFile(getOutputMediaFile(1));
                i.putExtra(MediaStore.EXTRA_OUTPUT, file2);
            } else if (recCode == 3){
                file3 = Uri.fromFile(getOutputMediaFile(1));
                i.putExtra(MediaStore.EXTRA_OUTPUT, file3);
            }

            i.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_BACK);
            i.putExtra("android.intent.extras.LENS_FACING_FRONT", Camera.CameraInfo.CAMERA_FACING_BACK);
            i.putExtra("android.intent.extra.USE_BACK_CAMERA", true);

            startActivityForResult(i,recCode);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[] {
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE },
                    102);
        }

    }

    public static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "KPH DATA");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        // Preparing media file naming convention
        // adds timestamp
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type ==0) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        switch(requestCode){
            case 1:
                if(resultCode == RESULT_OK){
                    fileString1 = compressImage(file1);
                    image1.setImageURI(Uri.parse(fileString1));
                    image1.requestFocus();
                    btn1.setText("Ganti Photo");
                }
                break;
            case 2:
                if(resultCode == RESULT_OK){
                    fileString2 = compressImage(file2);
                    image2.setImageURI(Uri.parse(fileString2));
                    image2.requestFocus();
                    btn2.setText("Ganti Photo");
                }
                break;
            case 3:
                if(resultCode == RESULT_OK){
                    fileString3 = compressImage(file3);
                    image3.setImageURI(Uri.parse(fileString3));
                    image3.requestFocus();
                    btn3.setText("Ganti Photo");
                }
                break;


        }
    }

    @OnClick(R.id.fab)
    public void clickFab(){
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            myLocation.getLocation(getActivity(), locationResult);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION },
                    101);
        }

    }

    @OnClick(R.id.btn_add_location)
    public void addLocation(){
        MapFragment nextFrag= new MapFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, nextFrag, "findThisFragment")
                .addToBackStack(null)
                .commit();
    }



}
