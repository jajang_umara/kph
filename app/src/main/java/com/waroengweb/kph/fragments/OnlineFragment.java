package com.waroengweb.kph.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.waroengweb.kph.HomeActivity;
import com.waroengweb.kph.LoginActivity;
import com.waroengweb.kph.R;
import com.waroengweb.kph.adapter.KphAdapter;
import com.waroengweb.kph.app.AppController;
import com.waroengweb.kph.config.Config;
import com.waroengweb.kph.config.Session;
import com.waroengweb.kph.database.entity.Kph;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnlineFragment extends Fragment {

    static RecyclerView recyclerView;
    private GridLayoutManager layoutmanager;
    public static List<Kph> kphList = new ArrayList<>();
    private ProgressDialog pd;
    public static KphAdapter wAdapter;

    public OnlineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_online, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutmanager=new GridLayoutManager(getActivity(),2);
        layoutmanager.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutmanager);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Kph kph = kphList.get(position);
                //Gson gson = new Gson();

                //Toast.makeText(getActivity(),gson.toJson(kph),Toast.LENGTH_SHORT).show();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                DialogFragment dialogFragment = new DetailKphFragments(kph);
                dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                dialogFragment.show(ft, "dialog");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading Data...");
        wAdapter = new KphAdapter(getActivity(),kphList);
        recyclerView.setAdapter(wAdapter);
        prepareKphData();
        return view;

    }

    void prepareKphData() {
        pd.show();
        StringRequest strReq = new StringRequest(Request.Method.POST, Config.BASE_URL + "api/list_kph", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                kphList.clear();
                try {
                    JSONArray rows = new JSONArray(response);
                    for (int i = 0; i < rows.length(); i++) {
                        JSONObject row = rows.getJSONObject(i);
                        Kph kph = new Kph();
                        kph.setFoto1(row.getString("photo1"));
                        kph.setQuantity(Float.valueOf(String.valueOf(row.getDouble("quantity"))));
                        kph.setNamaPotensi(row.getString("nama_potensi"));
                        kph.setSatuan(row.getString("satuan"));
                        kph.setUploaded(1);
                        kph.setJenisPotensi(row.getString("jenis_potensi"));
                        kph.setNamaLatin(row.getString("nama_latin"));
                        kph.setKetinggian(Float.valueOf(String.valueOf(row.getDouble("ketinggian"))));
                        kph.setDesa(row.getString("desa"));
                        kph.setKecamatan(row.getString("kecamatan"));
                        kph.setKabupaten(row.getString("kabupaten"));
                        kph.setTataBlok(row.getString("tata_blok"));
                        kph.setKodeBlok(row.getString("kode_blok"));
                        kphList.add(kph);
                    }
                    wAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                        e.printStackTrace();
                }

                pd.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                pd.hide();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", String.valueOf(Session.getUserId(getActivity())));


                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(strReq, "Str Req");
    }

}
